
// Playing Cards
// Jason Clemons
// Ryan Wheeler
//Forgot Notes

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum class Suit { //Enums represented by ints when compiled
	Hearts,
	Diamonds,
	Spades,
	Clubs
};

enum class Rank {
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};



struct Card { //Created data type
	Suit Suit;
	Rank Rank;
};

//Function prototype 
void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{

	//Created Card established values
	Card c1;
	c1.Rank = Rank::King;
	c1.Suit = Suit::Clubs;

	Card c2;
	c2.Rank = Rank::Ace; 
	c2.Suit = Suit::Spades;

	PrintCard(c1);
	HighCard(c1, c2);

	(void)_getch();
	return 0;
}

void PrintCard(Card card)
{
	//Card is passed to this function, function casts enum to int and stores in variable
	//inSuit/inRank are checked against cases and stores its matched value as a string
	int inRank = (int)card.Rank;
	int inSuit = (int)card.Suit;
	string ouRank = "";
	string ouSuit = "";

	switch (inRank)
	{
		case 14: ouRank = "Ace"; break;
		case 13: ouRank = "King"; break;
		case 12: ouRank = "Queen"; break;
		case 11: ouRank = "Jack"; break;
		case 10: ouRank = "Ten"; break;
		case 9: ouRank = "Nine"; break;
		case 8: ouRank = "Eight"; break;
		case 7: ouRank = "Seven"; break;
		case 6: ouRank = "Six"; break;
		case 5: ouRank = "Five"; break;
		case 4: ouRank = "Four"; break;
		case 3: ouRank = "Three"; break;
		case 2: ouRank = "Two"; break;
	}

	switch (inSuit)
	{
		case 0: ouSuit = "Hearts"; break;
		case 1: ouSuit = "Diamonds"; break;
		case 2: ouSuit = "Spades"; break;
		case 3: ouSuit = "Clubs"; break;
	}

	//Prints card
	cout << ouRank << " of " << ouSuit;

}

Card HighCard(Card card1, Card card2) //function gets passed the two cards, then compared and high card returned.
{
	//function gets passed the two cards, then compared and high card returned.
	//Note: did not create an if for cards being equal/suit tie breakers. Unsure if that was wanted.
	if ((int)card1.Rank < (int)card2.Rank)
	{

		return card2;
	}
	else
	{
		return card1;
	}
	
}